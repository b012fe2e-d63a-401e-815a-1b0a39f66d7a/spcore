package net.mintar.spigotcore.data;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import protocolsupport.api.ProtocolVersion;

import java.util.UUID;

public class MintarPlayer {

    private UUID playerUUID;
    private String username;
    private int level, coins;
    private Ranks ranks;
    private ProtocolVersion protocolVersion;

    public MintarPlayer(UUID uuid){
        this.playerUUID = uuid;
        this.username = Bukkit.getPlayer(uuid).getName();
        this.ranks = Ranks.DEFAULT;
        this.coins = 200;
        this.level = 1;
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public Ranks getRanks() {
        return ranks;
    }

    public void setRanks(Ranks ranks) {
        this.ranks = ranks;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Player getPlayer(){
        return Bukkit.getPlayer(this.playerUUID);
    }

    public ProtocolVersion getVersion() {
        return protocolVersion;
    }

    public void setVersion(ProtocolVersion protocolVersion) {
        this.protocolVersion = protocolVersion;
    }
}
