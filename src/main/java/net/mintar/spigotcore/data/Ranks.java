package net.mintar.spigotcore.data;

import net.md_5.bungee.api.ChatColor;

import java.util.Arrays;
import java.util.Optional;

public enum Ranks {

    // Staff
    OWNER("Owner", "&4&lOwner &4", ChatColor.RED, 10),
    ADMIN("Admin", "&c&lAdmin &c", ChatColor.YELLOW, 9),
    MODERATOR("Mod", "&3Mod &3", ChatColor.DARK_AQUA, 8),
    HELPER("Helper", "&2Helper &2", ChatColor.DARK_AQUA, 7),

    //Special
    YOUTUBER("YouTuber", "&4You&fTube &c", ChatColor.GOLD, 6),
    SMALLYT("YT", "&5YT &d", ChatColor.GOLD, 5),

    //Donator
    MONARCH("Monarch", "&eMonarch &e", ChatColor.WHITE, 4),
    BISHOP("Bishop", "&9Bishop &9", ChatColor.WHITE, 3),
    KNIGHT("Knight", "&aKnight &a", ChatColor.WHITE, 2),
    JESTER("Jester", "&dJester &d", ChatColor.WHITE, 1),

    //Everyone
    DEFAULT("Default", "&7Member &7", ChatColor.GRAY, 0);

    private String commandName, prefix;
    private ChatColor chatColor;
    private int rankLevel;

    Ranks(String commandName, String prefix, ChatColor chatColor, int rankLevel){
        this.commandName = commandName;
        this.prefix = prefix;
        this.chatColor = chatColor;
        this.rankLevel = rankLevel;
    }

    public String getCommandName() {
        return commandName;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getRankLevel() {
        return rankLevel;
    }

    public boolean isHigherOrEqualsTo(Ranks compareTo){
        return this.getRankLevel() >= compareTo.getRankLevel();
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public void setChatColor(ChatColor chatColor) {
        this.chatColor = chatColor;
    }

    public static Optional<Ranks> fromString(String ranks){
        return Arrays.stream(values()).filter(all -> all.toString().equalsIgnoreCase(ranks)).findFirst();
    }
}
