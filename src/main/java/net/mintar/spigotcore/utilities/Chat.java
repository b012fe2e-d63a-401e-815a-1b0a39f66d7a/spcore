package net.mintar.spigotcore.utilities;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Chat {

    public static String format(String input) {
        return ChatColor.translateAlternateColorCodes('&', input);
    }

    // TODO : Add rank param
    public static void broadcast(BroadcastLevel level, String message) {
        if (message == null) return;
        switch (level) {
            case OP:
                for(Player player : Bukkit.getOnlinePlayers()) {
                    if (player.isOp()) player.sendMessage(Chat.format(message));
                }
                break;
            case ALL:
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.sendMessage(Chat.format(message));
                }
                break;
        }
    }

    public enum BroadcastLevel { OP, ALL }

}
