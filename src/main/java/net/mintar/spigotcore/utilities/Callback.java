package net.mintar.spigotcore.utilities;

public interface Callback<D> {

    void call(D data);

    default void fail(String msg) {}

}
