package net.mintar.spigotcore.utilities;

import org.bukkit.ChatColor;

public class Messages {

    public static String PREFIX = "§3§lMintar§b§lGames §8» §b";
    public static String STAFF_PREFIX = "§c§lSTAFF §8» §b";
    public static String ADMINCOMMAND_TOADMINS = STAFF_PREFIX + "From {user}§7:§c {message}";
    public static String PLAYER_ONLY_COMMAND = PREFIX + "This command is player only.";
    public static String REGISTERED_ALL_COMMANDS = STAFF_PREFIX + "All commands have been successfully registered to the database.";
    public static String EXCEPTIONS = "§c§lERROR ({location}) §8» §c{error}";
    public static String CHAT_COOLDOWN = PREFIX + "You must wait 5 seconds before sending a new chat message.";
    public static String TOOMANYARGS = PREFIX + "You have specified too many arguments for this command.";
    public static String COMMAND_NOT_FUNCTIONAL = PREFIX + "This command is not functional at this time.";
    public static String NO_PERMISSION = PREFIX + "You do not have permission to do this.";

    public static String formatMessage(String message){
        return ChatColor.translateAlternateColorCodes('&', message);
    }

}
