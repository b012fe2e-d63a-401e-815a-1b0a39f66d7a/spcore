package net.mintar.spigotcore.utilities;

import java.sql.ResultSet;

public interface SelectCall {

    void call(ResultSet resultSet);

}
