package net.mintar.spigotcore.commands.staff;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HostEventCommand extends Command {

    public HostEventCommand() {
        super(Ranks.MODERATOR, "hostevent", "he");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player)commandSender;
        player.sendMessage(Messages.COMMAND_NOT_FUNCTIONAL);

        return false;
    }
}
