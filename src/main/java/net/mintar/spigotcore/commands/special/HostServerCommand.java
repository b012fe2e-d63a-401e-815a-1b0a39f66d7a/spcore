package net.mintar.spigotcore.commands.special;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HostServerCommand extends Command {

    public HostServerCommand() {
        super(Ranks.YOUTUBER, "hostserver", "hs", "mps");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player)commandSender;
        player.sendMessage(Messages.COMMAND_NOT_FUNCTIONAL);
        return false;
    }
}
