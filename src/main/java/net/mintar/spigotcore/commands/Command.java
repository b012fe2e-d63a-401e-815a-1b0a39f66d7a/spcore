package net.mintar.spigotcore.commands;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandMap;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class Command extends org.bukkit.command.Command implements Comparable<Command>, Executable {

    private final String name;
    private final Ranks rank;
    private final Set<String> aliases;
    private String usage;
    private int argsLength;

    public Command(Ranks ranks, String name, String... aliases) {
        this(ranks, 0, "", name, aliases);
    }

    public Command(Ranks rank, int argsLength, String usage, String name, String... aliases) {
        super(name, "", usage, Arrays.asList(aliases));

        this.name = name;
        this.rank = rank;
        this.argsLength = argsLength;
        this.usage = ChatColor.translateAlternateColorCodes('&', usage);

        this.aliases = new HashSet<>();
        this.aliases.add(name);
        Collections.addAll(this.aliases, aliases);

        registerBukkitCommand(aliases);
    }

    private void registerBukkitCommand(String[] aliases){
        try{
            final Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
            commandMap.register("command", this);
            for(String alias : aliases) {
                commandMap.register(alias, "command", this);
            }
        } catch (NoSuchFieldException  | IllegalArgumentException | IllegalAccessException exception){
            SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{error}", exception.getMessage()));
        }
    }

    @Override
    public int compareTo(Command o) {
        return o.getRank().getRankLevel();
    }

    @Override
    public Ranks getRank() {
        return this.rank;
    }

    @Override
    public int getArgsLength() {
        return this.argsLength;
    }

    @Override
    public boolean hasUsage() {
        return !this.usage.isEmpty();
    }

    @Override
    public Command setArgsLength(int argsLength) {
        this.argsLength = argsLength;
        return this;
    }

    @Override
    public Command setUsage(String usage) {
        this.usage = ChatColor.translateAlternateColorCodes('&', usage);
        return this;
    }

    @Override
    public Set<String> getNameList() {
        return this.aliases;
    }

    @Override
    public String getName() {
        return name;
    }
}
