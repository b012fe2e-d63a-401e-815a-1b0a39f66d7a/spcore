package net.mintar.spigotcore.commands.player;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpCommand extends Command {

    public HelpCommand() {
        super(Ranks.DEFAULT, "help", "support", "helpop", "adminshelpmepls");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player)commandSender;
        player.sendMessage(Messages.formatMessage(Messages.PREFIX + "&bFor help, please visit: &7&o&nhttps://www.mintar.net"));

        return false;
    }
}
