package net.mintar.spigotcore.commands.player;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PingCommand extends Command {

    public PingCommand() {
        super(Ranks.DEFAULT, "ping", "pong", "whatsmyping", "png");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {

        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player)commandSender;
        player.sendMessage(Messages.PREFIX + "Your ping is: §e" + ((CraftPlayer) player).getHandle().ping + " §bms");

        return false;
    }
}
