package net.mintar.spigotcore.commands.player;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReportCommand extends Command {

    public ReportCommand() {
        super(Ranks.MONARCH, "report", "cr", "rp");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player)commandSender;
        if(args.length <= 1){
            player.sendMessage(Messages.formatMessage(Messages.PREFIX + "&cThis command requires at least 2 arguments. The first one HAS to be a player."));
            return false;
        }



        return false;
    }
}
