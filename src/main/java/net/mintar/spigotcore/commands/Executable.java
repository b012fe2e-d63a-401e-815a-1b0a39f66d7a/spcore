package net.mintar.spigotcore.commands;

import net.mintar.spigotcore.data.Ranks;
import org.bukkit.command.CommandSender;

import java.util.Set;

public interface Executable {

    Ranks getRank();

    String getName();

    String getUsage();

    int getArgsLength();

    boolean hasUsage();

    Command setArgsLength(int argsLength);

    Command setUsage(String usage);

    Set<String> getNameList();

    boolean execute(CommandSender commandSender, String command, String[] args);

}
