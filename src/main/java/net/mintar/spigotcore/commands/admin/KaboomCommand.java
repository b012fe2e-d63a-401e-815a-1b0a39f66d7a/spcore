package net.mintar.spigotcore.commands.admin;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class KaboomCommand extends Command {

    public KaboomCommand() {
        super(Ranks.ADMIN, "Kaboom", "kboom", "kb", "boom");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player) commandSender;

        if(strings.length != 0){
            player.sendMessage(Messages.TOOMANYARGS);
            return false;
        }

        for(Player players : Bukkit.getOnlinePlayers()){
            players.setVelocity(new Vector(0, 20, 0));
            players.getLocation().getWorld().strikeLightningEffect(players.getLocation());
            players.sendMessage("§cKABOOM!");
        }

        return false;
    }
}
