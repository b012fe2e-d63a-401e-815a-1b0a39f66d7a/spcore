package net.mintar.spigotcore.commands.admin;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.MintarPlayer;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.managers.ScoreboardManager;
import net.mintar.spigotcore.utilities.Callback;
import net.mintar.spigotcore.utilities.Messages;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RanksCommand extends Command {

    public RanksCommand() {
        super(Ranks.ADMIN, "setrank", "ranks");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {

        if(args.length <= 1){
            commandSender.sendMessage(Messages.formatMessage(Messages.PREFIX + "&cThis command requires at least 2 arguments. The first one HAS to be a player."));
            return false;
        }

        Ranks rank = Ranks.fromString(args[1]).orElse(null);
        if(rank == null){
            commandSender.sendMessage(Messages.formatMessage(Messages.STAFF_PREFIX + "&cThat rank could not be found! Please try: &e" + StringUtils.join(Ranks.values(), ", ")));
            return false;
        }

        SpigotCore.getInstance().getPlayerManager().getAccountFromDatabaseByUsername(args[0], new Callback<MintarPlayer>() {
            @Override
            public void call(MintarPlayer data) {
                if(data == null){
                    commandSender.sendMessage(Messages.formatMessage(Messages.STAFF_PREFIX + "&cThat person has never logged into the server. Please try again."));
                    return;
                }

                SpigotCore.getInstance().getDatabaseManager().executeQuery("UPDATE Core_Players SET ranks=? WHERE playerUUID=?;", rank.toString(), data.getPlayerUUID());
                commandSender.sendMessage(Messages.formatMessage(Messages.STAFF_PREFIX + "&aSuccess! That player's account has been updated."));

                Player target = Bukkit.getPlayer(data.getPlayerUUID());
                if(target == null || !target.isOnline()){
                    return;
                }

                SpigotCore.getInstance().getPlayerManager().getPlayerAccount(target.getUniqueId()).setRanks(rank);
                target.sendMessage(Messages.formatMessage(Messages.PREFIX + "&aYour rank has been updated to: " + rank.getPrefix()));
                target.sendMessage(Messages.PREFIX + "Please rejoin to get the full effects of the rank!");

                Bukkit.getScheduler().runTask(SpigotCore.getInstance(), () -> {
                    ScoreboardManager.reset(target);
                    ScoreboardManager.setAll(target);
                });
            }

            @Override
            public void fail(String message){
                commandSender.sendMessage(Messages.formatMessage(Messages.STAFF_PREFIX + "There was an error, please try again: " + message));
            }
        });

        return false;
    }
}
