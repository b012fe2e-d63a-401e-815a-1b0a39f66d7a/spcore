package net.mintar.spigotcore.commands.admin;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SlimeballCommand extends Command {

    public SlimeballCommand() {
        super(Ranks.ADMIN, "slimeball", "ball", "slime");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {

        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player) commandSender;

        if(strings.length >=1 ){
            player.sendMessage(Messages.TOOMANYARGS);
            return false;
        }

        ItemStack itemStack = new ItemStack(Material.SLIME_BALL, 1);
        itemStack.addUnsafeEnchantment(Enchantment.KNOCKBACK, 10);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(Messages.formatMessage("&8[&c&lAdmin&8] &a&lKnockback X Slimeball."));
        itemStack.setItemMeta(itemMeta);

        player.getInventory().addItem(itemStack);

        return false;
    }
}
