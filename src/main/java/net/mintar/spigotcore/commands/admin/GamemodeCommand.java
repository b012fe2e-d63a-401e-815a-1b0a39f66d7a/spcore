package net.mintar.spigotcore.commands.admin;

import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GamemodeCommand extends Command {

    public GamemodeCommand() {
        super(Ranks.ADMIN, "gm", "g", "setmode");
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if(!(sender instanceof Player)){
            sender.sendMessage(Messages.PLAYER_ONLY_COMMAND);
            return false;
        }

        Player player = (Player) sender;

        if(args.length == 0){
            player.sendMessage(Messages.STAFF_PREFIX + "/gm creative");
            player.sendMessage(Messages.STAFF_PREFIX + "/gm survival");
            player.sendMessage(Messages.STAFF_PREFIX + "/gm spectator");
            player.sendMessage(Messages.STAFF_PREFIX + "/gm adventure");
            return false;
        }

        GameMode gamemode = GameMode.valueOf(args[0].toUpperCase());
        if(gamemode == null){
            player.sendMessage(Messages.STAFF_PREFIX + "That gamemode does not exist, did you spell it right?");
            return false;
        }

        if(args[0].equalsIgnoreCase("c")|| args[0].equalsIgnoreCase("s") || args[0].equalsIgnoreCase("a")) return false;

        if(args[0].equalsIgnoreCase("1")|| args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("2")) return false;

        player.setGameMode(gamemode);
        player.sendMessage(Messages.STAFF_PREFIX + "Your gamemode was updated to: " + gamemode.toString());

        return false;
    }
}
