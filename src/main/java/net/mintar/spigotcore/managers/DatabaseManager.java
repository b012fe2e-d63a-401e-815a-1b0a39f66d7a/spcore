package net.mintar.spigotcore.managers;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.utilities.Messages;
import net.mintar.spigotcore.utilities.SelectCall;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseManager {

    private HikariConfig hikariConfig;
    private HikariDataSource hikariDataSource;

    public Exception connectionException(){
        try{
            hikariConfig = new HikariConfig();
            hikariConfig.setJdbcUrl("jdbc:mysql://" + SpigotCore.getInstance().getConfig().getString("database.hostname") + ":" +
                    3306 + "/" + SpigotCore.getInstance().getConfig().getString("database.database"));
            hikariConfig.setUsername(SpigotCore.getInstance().getConfig().getString("database.username"));
            hikariConfig.setPassword(SpigotCore.getInstance().getConfig().getString("database.password"));
            hikariDataSource = new HikariDataSource(hikariConfig);
        }catch(Exception exception){
            SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{location}", "#connectionException").replace("{error}", exception.getMessage()));
            exception.printStackTrace();
            return exception;
        }
        return null;
    }

    public void close(){
        this.hikariDataSource.close();
    }

    public Connection getConnection() throws SQLException {
        return hikariDataSource.getConnection();
    }

    public void createTable(String name, String information){
        new Thread(() -> {
            try(Connection resource = getConnection(); PreparedStatement preparedStatement = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS " + name + "(" + information + ");")){
                preparedStatement.execute();
            }catch(SQLException exception){
                SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{location}", "#createTable").replace("{error}", exception.getMessage()));
                exception.printStackTrace();
            }
        }).start();
    }

    public void executeQuery(String query, Object... values){
        new Thread(() -> {
            try(Connection resource = getConnection(); PreparedStatement preparedStatement = resource.prepareStatement(query)){
                for(int i = 0; i < values.length; i++){
                    preparedStatement.setObject((i + 1), values[i]);
                }
                preparedStatement.execute();
            }catch(SQLException exception){
                SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{location}", "#executeQuery").replace("{error}", exception.getMessage()));
                exception.printStackTrace();
            }
        }).start();
    }

    public void select(String query, SelectCall callback, Object... values) {
        new Thread(() -> {
            try (Connection resource = getConnection(); PreparedStatement statement = resource.prepareStatement(query)) {
                for (int i = 0; i < values.length; i++) {
                    statement.setObject((i + 1), values[i]);
                }
                callback.call(statement.executeQuery());
            } catch (SQLException exception) {
                SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{location}", "#select").replace("{error}", exception.getMessage()));
                exception.printStackTrace();
            }
        }).start();
    }

}
