package net.mintar.spigotcore.managers;

import net.md_5.bungee.api.ChatColor;
import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.utilities.BlinkEffectAPI;
import net.mintar.spigotcore.utilities.TitleUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TablistManager {

    private static BlinkEffectAPI TAB_TITLE = new BlinkEffectAPI("MintarGames v2.0", ChatColor.DARK_AQUA, ChatColor.GRAY, ChatColor.AQUA);

    public static void start(){
        SpigotCore.getInstance().getServer().getScheduler().runTaskTimerAsynchronously(SpigotCore.getInstance(), () -> {
            String title = TAB_TITLE.next();
            for(Player player : Bukkit.getOnlinePlayers()){
                TitleUtil.sendTabTitle(player, title + "\n§bYou're connected to play.mintar.net","§3§lFORUMS: §bwww.mintar.net\n§3§lSTORE: §bstore.mintar.net\n§3§lDISCORD: §bdiscord.mintar.net");
            }
        },0, 2);
    }

}
