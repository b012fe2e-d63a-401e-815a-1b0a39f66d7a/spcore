package net.mintar.spigotcore.managers;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.data.MintarPlayer;
import net.mintar.spigotcore.utilities.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import protocolsupport.api.ProtocolVersion;

public class ScoreboardManager {

    public static void reset(Player player) {
        MintarPlayer MintarPlayer = SpigotCore.getInstance().getPlayerManager().getPlayerAccount(player.getUniqueId());
        if (MintarPlayer == null) {
            return;
        }
        for (Player online : Bukkit.getOnlinePlayers()) {
            Scoreboard onlineScoreboard = online.getScoreboard();
            Team team = onlineScoreboard.getTeam(player.getName());
            if (team == null) {
                continue;
            }
            team.unregister();
        }
    }

    public static void setAll(Player player) {

        MintarPlayer MintarPlayer = SpigotCore.getInstance().getPlayerManager().getPlayerAccount(player.getUniqueId());
        if(MintarPlayer == null){
            return;
        }

        if (MintarPlayer == null) {
            // No data.
            return;
        }

        // Set a new scoreboard for the player.
        MintarPlayer.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());

        // Setting the prefix for the player himself.
        Scoreboard scoreboard = MintarPlayer.getPlayer().getScoreboard();

        Team customTeam = scoreboard.registerNewTeam(MintarPlayer.getPlayer().getName());
        // Using ChatColor#stripColor to check for a "just a color"-prefix. e.g. "&6".

        if (MintarPlayer.getVersion().isBefore(ProtocolVersion.MINECRAFT_1_8)) {
            customTeam.setPrefix(Chat.format(MintarPlayer.getRanks().getPrefix().substring(0, 2)));
        } else {
            customTeam.setPrefix(Chat.format(ChatColor.stripColor(Chat.format(MintarPlayer.getRanks().getPrefix())).length() == 0
                    ? MintarPlayer.getRanks().getPrefix() : (MintarPlayer.getRanks().getPrefix() + "")));

        }

        customTeam.addEntry(MintarPlayer.getPlayer().getName());

        // Setting the prefix for all other online players.
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (online.equals(MintarPlayer.getPlayer())) {
                // We don't want to set the prefix on the player's scoreboard twice.
                continue;
            }

            MintarPlayer onlineMintarPlayer = SpigotCore.getInstance().getPlayerManager().getPlayerAccount(MintarPlayer.getPlayer().getUniqueId());
            if (onlineMintarPlayer == null) {
                continue;
            }

            Scoreboard onlineScoreboard = online.getScoreboard();
            Team customPlayerTeam = onlineScoreboard.registerNewTeam(MintarPlayer.getPlayer().getName());

            if (onlineMintarPlayer.getVersion().isBefore(ProtocolVersion.MINECRAFT_1_8)) {
                customPlayerTeam.setPrefix(Chat.format(MintarPlayer.getRanks().getPrefix().substring(0, 2)));
            } else {
                customPlayerTeam.setPrefix(Chat.format(ChatColor.stripColor(Chat.format(onlineMintarPlayer.getRanks().getPrefix())).length() == 0
                        ? onlineMintarPlayer.getRanks().getPrefix() : (onlineMintarPlayer.getRanks().getPrefix() + "")));

            }

            customPlayerTeam.addEntry(MintarPlayer.getPlayer().getName());

            Team customOnlineTeam = scoreboard.registerNewTeam(online.getName());

            if (MintarPlayer.getVersion().isBefore(ProtocolVersion.MINECRAFT_1_8)) {
                customOnlineTeam.setPrefix(Chat.format(onlineMintarPlayer.getRanks().getPrefix().substring(0, 2)));
            } else {
                customOnlineTeam.setPrefix(Chat.format(ChatColor.stripColor(Chat.format(onlineMintarPlayer.getRanks().getPrefix())).length() == 0
                        ? onlineMintarPlayer.getRanks().getPrefix() : (onlineMintarPlayer.getRanks().getPrefix() + "")));

            }

            customOnlineTeam.addEntry(online.getName());
        }
    }
    
}
