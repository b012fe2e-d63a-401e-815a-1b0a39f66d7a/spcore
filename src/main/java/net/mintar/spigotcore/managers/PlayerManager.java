package net.mintar.spigotcore.managers;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.data.MintarPlayer;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Callback;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import protocolsupport.api.ProtocolSupportAPI;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

public class PlayerManager {

    public final Collection<MintarPlayer> onlinePlayers = new HashSet<>();

    public void addPlayer(MintarPlayer mintarPlayer) {
        onlinePlayers.add(mintarPlayer);
    }

    public void removePlayer(MintarPlayer mintarPlayer) {
        onlinePlayers.remove(mintarPlayer);
    }

    public MintarPlayer getPlayerAccount(UUID uuid){
        for(MintarPlayer mintarPlayer : onlinePlayers){
            if(mintarPlayer.getPlayerUUID().equals(uuid)){
                return mintarPlayer;
            }
        }
        return null;
    }

    public void getAccountFromDatabaseByUUID(UUID uuid, Callback<MintarPlayer> callback){
        SpigotCore.getInstance().getDatabaseManager().select("SELECT * FROM Core_Players WHERE playerUUID=?;", resultSet -> {
            try{
                MintarPlayer mintarPlayer;
                if(resultSet.next()){
                    mintarPlayer = new MintarPlayer(uuid);
                    mintarPlayer.setCoins(resultSet.getInt("coins"));
                    mintarPlayer.setRanks(Ranks.valueOf(resultSet.getString("ranks")));
                    mintarPlayer.setLevel(resultSet.getInt("level"));
                    mintarPlayer.setUsername(resultSet.getString("username"));
                }else{
                    mintarPlayer = null;
                }
                callback.call(mintarPlayer);
            }catch(SQLException exception){
                SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{error}", exception.getMessage()));
            }
        }, uuid.toString());
    }

    public void getAccountFromDatabaseByUsername(String name, Callback<MintarPlayer> callback){
        SpigotCore.getInstance().getDatabaseManager().select("SELECT * FROM Core_Players WHERE username=?;", resultSet -> {
            try{
                MintarPlayer mintarPlayer;
                if(resultSet.next()){
                    mintarPlayer = new MintarPlayer(Bukkit.getPlayer(name).getUniqueId());
                    mintarPlayer.setCoins(resultSet.getInt("coins"));
                    mintarPlayer.setRanks(Ranks.valueOf(resultSet.getString("ranks")));
                    mintarPlayer.setLevel(resultSet.getInt("level"));
                }else{
                    mintarPlayer = null;
                }
                callback.call(mintarPlayer);
            }catch(SQLException exception){
                SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{error}", exception.getMessage()));
            }
        }, name);
    }

    public void getAccountFromDatabaseByUUIDOrInsertNewAccount(Player player, Callback<MintarPlayer> callback){
        SpigotCore.getInstance().getDatabaseManager().select("SELECT * FROM Core_Players WHERE playerUUID=?;", resultSet -> {
            try{
                MintarPlayer mintarPlayer;
                if(resultSet.next() && resultSet.getString("ranks") != null){
                    mintarPlayer = new MintarPlayer(player.getUniqueId());
                    mintarPlayer.setUsername(resultSet.getString("username"));
                    mintarPlayer.setCoins(resultSet.getInt("coins"));
                    mintarPlayer.setLevel(resultSet.getInt("level"));
                    mintarPlayer.setRanks(Ranks.valueOf(resultSet.getString("ranks")));
                    mintarPlayer.setVersion(ProtocolSupportAPI.getProtocolVersion(player));
                }else{
                    mintarPlayer = new MintarPlayer(player.getUniqueId());
                    mintarPlayer.setRanks(Ranks.DEFAULT);
                    mintarPlayer.setLevel(1);
                    mintarPlayer.setCoins(1000);
                    mintarPlayer.setUsername(player.getName());
                    mintarPlayer.setVersion(ProtocolSupportAPI.getProtocolVersion(player));

                    insertAccountIntoTheDatabase(mintarPlayer);
                }
                callback.call(mintarPlayer);
            }catch(SQLException exception){
                SpigotCore.getInstance().getServer().getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{error}", exception.getMessage()));
            }
        },player.getUniqueId().toString());
    }

    private void insertAccountIntoTheDatabase(MintarPlayer mintarPlayer){
        SpigotCore.getInstance().getDatabaseManager().executeQuery("INSERT INTO Core_Players(playerUUID, ranks, coins, level, username) " +
                "VALUES(?,?,?,?,?) ON DUPLICATE KEY UPDATE ranks=?, username=?, coins=?, level=?;",

                mintarPlayer.getPlayerUUID().toString(),
                mintarPlayer.getRanks().toString(),
                mintarPlayer.getCoins(),
                mintarPlayer.getLevel(),
                mintarPlayer.getUsername(),

                mintarPlayer.getRanks().toString(),
                mintarPlayer.getUsername(),
                mintarPlayer.getCoins(),
                mintarPlayer.getLevel()
        );
    }
}


   // INSERT INTO Core_Players(playerUUID, ranks, coins, level, username) VALUES('f010ee33-d9fa-4044-84a7-130084a065af','Owner','1000','100','NetworkControl');