package net.mintar.spigotcore.managers;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.commands.admin.GamemodeCommand;
import net.mintar.spigotcore.commands.admin.KaboomCommand;
import net.mintar.spigotcore.commands.admin.RanksCommand;
import net.mintar.spigotcore.commands.admin.SlimeballCommand;
import net.mintar.spigotcore.commands.player.HelpCommand;
import net.mintar.spigotcore.commands.player.PingCommand;
import net.mintar.spigotcore.commands.special.HostServerCommand;
import net.mintar.spigotcore.commands.staff.HostEventCommand;
import net.mintar.spigotcore.utilities.Messages;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class CommandManager {

    private static Set<Command> commands = new HashSet<>();

    public static void registerAllCommands(){
        commands.addAll(Arrays.asList(
                new GamemodeCommand(),
                new KaboomCommand(),
                new HostServerCommand(),
                new HostEventCommand(),
                new PingCommand(),
                new SlimeballCommand(),
                new HelpCommand(),
                new RanksCommand()
        ));
        SpigotCore.getPlugin(SpigotCore.class).getServer().getConsoleSender().sendMessage(Messages.REGISTERED_ALL_COMMANDS);
    }

    public static void register(Command... command) {
        commands.addAll(Arrays.asList(command));
    }

    public static Optional<Command> byCommand(String command) {
        return commands.stream().filter(all -> {
            if (all.getName().equalsIgnoreCase(command)) {
                return true;
            } else {
                for (String alias : all.getAliases()) {
                    if (alias.equalsIgnoreCase(command)) {
                        return true;
                    }
                }
                return false;
            }
        }).findFirst();
    }

}
