package net.mintar.spigotcore.listeners;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.*;

public class ChatEvent implements Listener {

    Map<UUID, Long> playersInCooldown = new HashMap<>();
    private int cooldown = 5;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event){
        if(!SpigotCore.getInstance().getPlayerManager().getPlayerAccount(event.getPlayer().getUniqueId()).getRanks().isHigherOrEqualsTo(Ranks.JESTER)){
           if(playersInCooldown.containsKey(event.getPlayer().getUniqueId())){
               long timeleft = ((playersInCooldown.get(event.getPlayer().getUniqueId()) / 1000) + cooldown - (System.currentTimeMillis()));
               if(timeleft >= 1){
                   event.getPlayer().sendMessage(Messages.CHAT_COOLDOWN);
                   event.setCancelled(true);
               }else{
                   playersInCooldown.remove(event.getPlayer().getUniqueId());
                   event.setCancelled(false);
               }
           }else{
               playersInCooldown.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
           }
        }
        event.setFormat(Messages.formatMessage(SpigotCore.getInstance().getPlayerManager().getPlayerAccount(event.getPlayer().getUniqueId()).getRanks().getPrefix() +
                event.getPlayer().getName() + " &8» &f%2$s"));
    }

}
