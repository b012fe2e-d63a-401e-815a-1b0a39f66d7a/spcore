package net.mintar.spigotcore.listeners;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.data.MintarPlayer;
import net.mintar.spigotcore.data.Ranks;
import net.mintar.spigotcore.managers.ScoreboardManager;
import net.mintar.spigotcore.utilities.Callback;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import protocolsupport.api.ProtocolSupportAPI;

public class JoinQuitListener implements Listener {

    @EventHandler
    public void onLogin(PlayerJoinEvent event){
        SpigotCore.getInstance().getPlayerManager().getAccountFromDatabaseByUUIDOrInsertNewAccount(event.getPlayer(), new Callback<MintarPlayer>() {
            @Override
            public void call(MintarPlayer data) {

                SpigotCore.getInstance().getServer().getScheduler().runTask(SpigotCore.getInstance(), () -> {
                    data.setVersion(ProtocolSupportAPI.getProtocolVersion(event.getPlayer()));

                });
                SpigotCore.getInstance().getPlayerManager().addPlayer(data);

                SpigotCore.getInstance().getServer().getScheduler().runTask(SpigotCore.getInstance(), () -> {
                    if(data.getRanks().isHigherOrEqualsTo(Ranks.HELPER)){
                        data.getPlayer().setAllowFlight(true);
                        data.getPlayer().setFlying(true);
                        data.getPlayer().sendMessage(Messages.STAFF_PREFIX + "Welcome back. Please remember to log punishments in discord.");
                        data.getPlayer().sendMessage(Messages.STAFF_PREFIX + "Your Flight has also been enabled automatically.");
                    }
                    ScoreboardManager.setAll(data.getPlayer());
                });
            }

            @Override
            public void fail(String msg) {
                Bukkit.getScheduler().runTask(SpigotCore.getInstance(), () -> event.getPlayer().kickPlayer(ChatColor.translateAlternateColorCodes('&',"&cCould not load your account:&r\n&f" + msg)));
            }
        });

        event.setJoinMessage(null);
    }

}
