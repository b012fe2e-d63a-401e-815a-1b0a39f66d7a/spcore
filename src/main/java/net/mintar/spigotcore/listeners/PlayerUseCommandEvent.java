package net.mintar.spigotcore.listeners;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.commands.Command;
import net.mintar.spigotcore.data.MintarPlayer;
import net.mintar.spigotcore.managers.CommandManager;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;

public class PlayerUseCommandEvent implements Listener {

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event){
        String[] args = event.getMessage().split(" ");
        Command command = CommandManager.byCommand(args[0].replace("/", "")).orElse(null);
        MintarPlayer mintarPlayer = SpigotCore.getInstance().getPlayerManager().getPlayerAccount(event.getPlayer().getUniqueId());

        if(command == null){
            HelpTopic helpTopic = SpigotCore.getInstance().getServer().getHelpMap().getHelpTopic(args[0]);
            if(helpTopic == null){
                event.setCancelled(true);
            }
            return;
        }

        if(mintarPlayer == null){
            event.getPlayer().kickPlayer("There was an error processing your request. Please try again.");
            event.setCancelled(true);
            return;
        }

        if(!mintarPlayer.getRanks().isHigherOrEqualsTo(command.getRank())){
            mintarPlayer.getPlayer().sendMessage(Messages.NO_PERMISSION);
            event.setCancelled(true);
            return;
        }
    }

}
