package net.mintar.spigotcore.listeners;

import net.mintar.spigotcore.SpigotCore;
import net.mintar.spigotcore.data.Ranks;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class PlayerDeathEvent implements Listener {

    @EventHandler
    public void onDeath(EntityDeathEvent event){
        if(!event.getEntityType().equals(EntityType.PLAYER)) return;
        Player player = (Player) event.getEntity();
        if(!player.getKiller().getType().equals(EntityType.PLAYER)) return;
        Player killer = (Player) player.getKiller();
        if(SpigotCore.getInstance().getPlayerManager().getPlayerAccount(killer.getUniqueId()).getRanks().isHigherOrEqualsTo(Ranks.JESTER)){
            player.getLocation().getWorld().strikeLightningEffect(player.getLocation());
        }
        killer.sendMessage("§cYou meme'd " + player.getName() + "! Well done!");
        player.sendMessage("§cYou got meme'd by " + killer.getName() + "! §c§lOOF§c!");
    }


}
