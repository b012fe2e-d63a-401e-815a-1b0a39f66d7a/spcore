package net.mintar.spigotcore;

import net.mintar.spigotcore.listeners.ChatEvent;
import net.mintar.spigotcore.listeners.JoinQuitListener;
import net.mintar.spigotcore.listeners.PlayerDeathEvent;
import net.mintar.spigotcore.listeners.PlayerUseCommandEvent;
import net.mintar.spigotcore.managers.CommandManager;
import net.mintar.spigotcore.managers.DatabaseManager;
import net.mintar.spigotcore.managers.PlayerManager;
import net.mintar.spigotcore.managers.TablistManager;
import net.mintar.spigotcore.utilities.Messages;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;


public final class SpigotCore extends JavaPlugin {

    private static SpigotCore instance;
    private DatabaseManager databaseManager;
    private PlayerManager playerManager;

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        databaseManager = new DatabaseManager();

        Exception connectionException = databaseManager.connectionException();
        if(connectionException != null){
            Bukkit.getConsoleSender().sendMessage(Messages.EXCEPTIONS.replace("{location}", "database #onEnable").replace("{error}", connectionException.getMessage()));
            Bukkit.shutdown();
            return;
        }
        databaseManager.createTable("Core_Players", "playerUUID VARCHAR(36) NOT NULL PRIMARY KEY, ranks TEXT, coins INT DEFAULT 200, level INT DEFAULT 1, username TEXT");

        playerManager = new PlayerManager();
        CommandManager.registerAllCommands();
        TablistManager.start();
        registerListeners(
                new JoinQuitListener(),
                new PlayerUseCommandEvent(),
                new PlayerDeathEvent(),
                new ChatEvent()
        );
    }

    @Override
    public void onDisable() {

    }

    public static SpigotCore getInstance() {
        return instance;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    private void registerListeners(Listener... listeners){
        for(Listener list : listeners){
            getServer().getPluginManager().registerEvents(list, getInstance());
        }
    }
}
